       IDENTIFICATION DIVISION.
       PROGRAM-ID. MATCHING-SORT-DATA.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      ************************  INPUT FILE *****************************
           SELECT 100-INPUT-FILE ASSIGN
               TO "Requirement/INPUT01_Incomes.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-100-FILE-STATUS.
           SELECT 200-INPUT-FILE ASSIGN
               TO "Requirement/INPUT02_Merchants.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-200-FILE-STATUS.
           SELECT 300-INPUT-FILE ASSIGN
               TO "Requirement/INPUT03_Contacts.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-300-FILE-STATUS.
           SELECT 400-INPUT-FILE ASSIGN
               TO "Requirement/INPUT04_Customers.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-400-FILE-STATUS.
           SELECT 500-INPUT-FILE ASSIGN
               TO "Requirement/INPUT05_Cards.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-500-FILE-STATUS.


           SELECT 700-OUTPUT-FILE ASSIGN
               TO "Requirement/CustomerStatement1.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-700-FILE-STATUS.
           SELECT 800-OUTPUT-FILE ASSIGN
               TO "Requirement/CustomerStatement2.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-800-FILE-STATUS.
           SELECT 900-OUTPUT-FILE ASSIGN
               TO "Requirement/CustomerStatement3.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-900-FILE-STATUS.

      *>      SELECT 600-OUTPUT-FILE ASSIGN
      *>          TO "Requirement/MATCHING-FILE.txt"
      *>          ORGANIZATION IS LINE SEQUENTIAL
      *>          FILE STATUS IS WS-600-FILE-STATUS.

      ************************  INPUT FILE *****************************
       DATA DIVISION.
       FILE SECTION.
      ***********************   INCOMEs    *****************************
       FD 100-INPUT-FILE.
       01 100-INPUT-RECORD.
           05 100-CARD-NUMBER              PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-NUMBER          PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-AMOUNT                   PIC 9(7)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MDR                      PIC 9(6)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-VAT                      PIC 9(5)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-TYPE                PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-TXN-DATE                 PIC X(8)        VALUE SPACE.
      ***********************   INCOMEs    *****************************

      ***********************   MERCHANTs  *****************************
       FD 200-INPUT-FILE.
       01 200-INPUT-RECORD.
           05 200-PROFILE                  PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-MERCHANT-NUMBER          PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-MERCHANT-NAME            PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-TAX-ID                   PIC X(14)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-MCC                      PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-CHAIN-MERCHANT-NUM       PIC X(21)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 200-MERCHANT-ACCOUNT         PIC X(16)       VALUE SPACE.
      ***********************   MERCHANTs  *****************************

      ***********************   CONTACTs   *****************************
       FD 300-INPUT-FILE.
       01 300-INPUT-RECORD.
           05 300-PROFILE                  PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 300-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 300-MERCHANT-ADDRESS-1       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 300-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 300-ZIP-CODE                 PIC X(8)        VALUE SPACE.
      ***********************   CONTACTs   *****************************

      ***********************   CUSTOMERs  *****************************
       FD 400-INPUT-FILE.
       01 400-INPUT-RECORD.
           05 400-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 400-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 400-PHONE-NUMBER             PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 400-ADDRESS-1                PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 400-ADDRESS-2                PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 400-ZIP-CODE                 PIC X(8)        VALUE SPACE.
      ***********************   CUSTOMERs  *****************************

      *************************   CARDs    *****************************
       FD 500-INPUT-FILE.
       01 500-INPUT-RECORD.
           05 500-CARD-NUMBER              PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 500-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 500-CARD-TYPE                PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 500-EXP-DATE                 PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 500-SECURITY-CODE            PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 500-CREDIT-LIMIT             PIC 9(8)V9(2)   VALUE ZERO.
      *************************   CARDs    *****************************

      *************************   Match    *****************************
      *>  FD 600-OUTPUT-FILE.
      *>  01 600-MATCH-RECORD                 PIC X(400)      VALUE SPACE.
       FD 700-OUTPUT-FILE.
       01 700-MATCH-RECORD                 PIC X(400)      VALUE SPACE.
       FD 800-OUTPUT-FILE.
       01 800-MATCH-RECORD                 PIC X(400)      VALUE SPACE.
       FD 900-OUTPUT-FILE.
       01 900-MATCH-RECORD                 PIC X(400)      VALUE SPACE.
      *************************   Match    *****************************

       WORKING-STORAGE SECTION.
       01 WS-FILE-STATUS.
           05  WS-100-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-200-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-300-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-400-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-500-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-700-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-800-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-900-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.

      *************************   CALCULATORs **************************
       01 WS-CAL.
           05 WS-REC-COUNT-FILE-1          PIC 9(4)        VALUE ZERO.
           05 WS-REC-COUNT-FILE-2          PIC 9(4)        VALUE ZERO.
           05 WS-REC-COUNT-FILE-3          PIC 9(4)        VALUE ZERO.
           05 WS-REC-COUNT-FILE-4          PIC 9(4)        VALUE ZERO.
           05 WS-REC-COUNT-FILE-5          PIC 9(4)        VALUE ZERO.

           05 WS-OUTPUT-FILE-1             PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE-2             PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE-3             PIC 9(4)        VALUE ZERO.

           05 WS-SUB                       PIC 9(4)        VALUE ZERO.

           05 WS-ARRAY-MAX-3               PIC 9(4)        VALUE 5.
           05 WS-ARRAY-MAX-4               PIC 9(4)        VALUE 7.
           05 WS-ARRAY-MAX                 PIC 9(4)        VALUE 15.
      *************************   CALCULATORs **************************

      ************************* ARRAY-CONTACT **************************
       01 WS-ARRAY-CONTACT                 OCCURS 10 INDEXED BY WS-ID-1.
           05 WSA-PROFILE                  PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-MERCHANT-ADDRESS-1       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-ZIP-CODE                 PIC X(8)        VALUE SPACE.
      ************************* ARRAY-CONTACT **************************

      ***********************  ARRAY-CUSTOMER **************************
       01 WS-ARRAY-CUSTOMER                OCCURS 10 INDEXED BY WS-ID-4.
           05 WSA-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-PHONE-NUMBER             PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-ADDRESS-1                PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-ADDRESS-2                PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WSA-CUS-ZIP-CODE             PIC X(8)        VALUE SPACE.
      ***********************  ARRAY-CUSTOMER **************************

      **************   MATCHING FILE-1 ARRAY-FILE-2-3-4-5   ************
       01 WS-ARRAY-MATCHING-F1F2F3         OCCURS 17 INDEXED BY WS-ID-3.
           05 WS-CARD-NUMBER-F1F2F3        PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-NUMBER-F1F2F3    PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-AMOUNT-F1F2F3             PIC 9(7)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MDR-F1F2F3                PIC 9(6)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-VAT-F1F2F3                PIC 9(5)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CARD-TYPE-F1F2F3          PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-TXN-DATE-F1F2F3           PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-PROFILE-F1F2F3            PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-NAME-F1F2F3      PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-TAX-ID-F1F2F3             PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MCC-F1F2F3                PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CHAIN-MERCHANT-NUM-F1F2F3 PIC X(21)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-CONTACT-F1F2F3   PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-ACCOUNT-F1F2F3   PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-ADDRESS-1-F1F2F3 PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-ADDRESS-2-F1F2F3 PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-ZIP-CODE-F1F2F3           PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 WS-CUSTOMER-ID-F4F5          PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CUSTOMER-NAME-F4F5        PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-PHONE-NUMBER-F4F5         PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-ADDRESS-1-F4F5            PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-ADDRESS-2-F4F5            PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-ZIP-CODE-F4F5             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 WS-CARD-NUMBER-F4F5          PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CARD-TYPE-F4F5            PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-EXP-DATE-F4F5             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-SECURITY-CODE-F4F5        PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CREDIT-LIMIT-F4F5         PIC 9(8)V9(2)   VALUE ZERO.



      ************   MATCHING FILE-1 ARRAY-FILE-2-3-4-5   **************

      *********************   MATCHING FILE-2 FILE-3   *****************
       01 WS-ARRAY-MATCHING-F2F3           OCCURS 10 INDEXED BY WS-ID-2.
           05 WS-PROFILE-F2F3              PIC X(15)       VALUE SPACE.
           05 WS-MERCHANT-NUMBER-F2F3      PIC X(15)       VALUE SPACE.
           05 WS-MERCHANT-NAME-F2F3        PIC X(14)       VALUE SPACE.
           05 WS-TAX-ID-F2F3               PIC X(13)       VALUE SPACE.
           05 WS-MCC-F2F3                  PIC X(4)        VALUE SPACE.
           05 WS-CHAIN-MERCHANT-NUM-F2F3   PIC X(21)       VALUE SPACE.
           05 WS-MERCHANT-CONTACT-F2F3     PIC X(16)       VALUE SPACE.
           05 WS-MERCHANT-ACCOUNT-F2F3     PIC X(16)       VALUE SPACE.
           05 WS-MERCHANT-ADDRESS-1-F2F3   PIC X(18)       VALUE SPACE.
           05 WS-MERCHANT-ADDRESS-2-F2F3   PIC X(18)       VALUE SPACE.
           05 WS-ZIP-CODE-F2F3             PIC X(8)        VALUE SPACE.
      *********************   MATCHING FILE-2 FILE-3   *****************

      *********************   MATCHING FILE-4 FILE-5   *****************
       01 WS-ARRAY-MATCHING-F4F5           OCCURS 10 INDEXED BY WS-ID-5.
           05 WSA-CUSTOMER-ID-F4F5          PIC X(15)       VALUE SPACE.
           05 WSA-CUSTOMER-NAME-F4F5        PIC X(30)       VALUE SPACE.
           05 WSA-PHONE-NUMBER-F4F5         PIC X(12)       VALUE SPACE.
           05 WSA-ADDRESS-1-F4F5            PIC X(12)       VALUE SPACE.
           05 WSA-ADDRESS-2-F4F5            PIC X(10)       VALUE SPACE.
           05 WSA-ZIP-CODE-F4F5             PIC X(8)        VALUE SPACE.

           05 WSA-CARD-NUMBER-F4F5          PIC X(16)       VALUE SPACE.
           05 WSA-CARD-TYPE-F4F5            PIC X(9)        VALUE SPACE.
           05 WSA-EXP-DATE-F4F5             PIC X(8)        VALUE SPACE.
           05 WSA-SECURITY-CODE-F4F5        PIC X(13)       VALUE SPACE.
           05 WSA-CREDIT-LIMIT-F4F5         PIC 9(8)V9(2)   VALUE ZERO.
      *********************   MATCHING FILE-2 FILE-3   *****************

       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITIAL            THRU 1000-EXIT.
           PERFORM 2000-PROCESS            THRU 2000-EXIT.
           PERFORM 3000-END                THRU 3000-EXIT.
           GOBACK.
       1000-INITIAL.
           OPEN INPUT  100-INPUT-FILE
                       200-INPUT-FILE
                       300-INPUT-FILE
                       400-INPUT-FILE
                       500-INPUT-FILE
                OUTPUT 700-OUTPUT-FILE
                       800-OUTPUT-FILE
                       900-OUTPUT-FILE.

           PERFORM 8000-READ-FILE-1        THRU 8000-EXIT.
           PERFORM 8100-READ-FILE-2        THRU 8100-EXIT.
           PERFORM 8200-READ-FILE-3        THRU 8200-EXIT.
           PERFORM 8300-READ-FILE-4        THRU 8300-EXIT.
           PERFORM 8400-READ-FILE-5        THRU 8400-EXIT.

           PERFORM 4000-LOAD-ARRAY-FILE-3  THRU 4000-EXIT.
           PERFORM 5000-LOAD-ARRAY-FILE-4  THRU 5000-EXIT.


       1000-EXIT.
           EXIT.
       2000-PROCESS.
           MOVE ZERO                       TO WS-SUB.
           PERFORM 4500-MATCHING-MC        THRU 4500-EXIT
                   UNTIL FILE-AT-END       OF WS-200-FILE-STATUS.
           MOVE ZERO                       TO WS-SUB.
           PERFORM 5500-MATCHING-CC        THRU 5500-EXIT
                   UNTIL FILE-AT-END       OF WS-500-FILE-STATUS.
           MOVE ZERO                       TO WS-SUB.
           PERFORM 6000-MATCHING-ALL       THRU 6000-EXIT
                   UNTIL FILE-AT-END       OF WS-100-FILE-STATUS.
           SORT WS-ARRAY-MATCHING-F1F2F3   DESCENDING
                                           WS-CUSTOMER-ID-F4F5
                                           WS-CARD-NUMBER-F1F2F3.
           MOVE ZERO                       TO WS-SUB.

           PERFORM VARYING WS-SUB          FROM +1 BY +1
               UNTIL WS-SUB > WS-ARRAY-MAX
               EVALUATE WS-CUSTOMER-ID-F4F5(WS-SUB)
                   WHEN "CUSTOMER0000001"
                       MOVE WS-ARRAY-MATCHING-F1F2F3(WS-SUB)
                                                   TO 700-MATCH-RECORD
                       PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
                   WHEN "CUSTOMER0000002"
                       MOVE WS-ARRAY-MATCHING-F1F2F3(WS-SUB)
                                                   TO 800-MATCH-RECORD
                       PERFORM 7100-WRITE-FILE-2   THRU 7100-EXIT
                   WHEN "CUSTOMER0000003"
                       MOVE WS-ARRAY-MATCHING-F1F2F3(WS-SUB)
                                                   TO 900-MATCH-RECORD
                       PERFORM 7200-WRITE-FILE-3   THRU 7200-EXIT
                   WHEN OTHER
                       DISPLAY "ERROR : "
               END-EVALUATE
           END-PERFORM.

       2000-EXIT.
           EXIT.
       3000-END.
           CLOSE   100-INPUT-FILE
                   200-INPUT-FILE
                   300-INPUT-FILE
                   400-INPUT-FILE
                   500-INPUT-FILE

                   700-OUTPUT-FILE
                   800-OUTPUT-FILE
                   900-OUTPUT-FILE.

           DISPLAY "FILE 1 : " WS-REC-COUNT-FILE-1.
           DISPLAY "FILE 2 : " WS-REC-COUNT-FILE-2.
           DISPLAY "FILE 3 : " WS-REC-COUNT-FILE-3.
           DISPLAY "FILE 4 : " WS-REC-COUNT-FILE-4.
           DISPLAY "FILE 5 : " WS-REC-COUNT-FILE-5.

           DISPLAY "FILE 7 : " WS-OUTPUT-FILE-1.
           DISPLAY "FILE 8 : " WS-OUTPUT-FILE-2.
           DISPLAY "FILE 9 : " WS-OUTPUT-FILE-3.
       3000-EXIT.
           EXIT.
       4000-LOAD-ARRAY-FILE-3.
           MOVE ZERO                       TO WS-SUB.
           PERFORM VARYING WS-SUB          FROM +1 BY +1
               UNTIL FILE-AT-END           OF WS-300-FILE-STATUS
               OR WS-SUB >= WS-ARRAY-MAX-3
                  MOVE 300-INPUT-RECORD    TO WS-ARRAY-CONTACT(WS-SUB)
                  PERFORM 8200-READ-FILE-3 THRU 8200-EXIT
           END-PERFORM.

           IF WS-SUB > WS-ARRAY-MAX-3
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF.
           MOVE ZERO                       TO WS-SUB.
       4000-EXIT.
           EXIT.
       4500-MATCHING-MC.
           SEARCH WS-ARRAY-CONTACT
               WHEN WSA-MERCHANT-CONTACT(WS-ID-1) = 200-MERCHANT-CONTACT
                ADD 1                      TO WS-SUB
                MOVE 200-PROFILE
                           TO WS-PROFILE-F2F3(WS-SUB)
                MOVE 200-MERCHANT-NUMBER
                           TO WS-MERCHANT-NUMBER-F2F3(WS-SUB)
                MOVE 200-MERCHANT-NAME
                           TO WS-MERCHANT-NAME-F2F3(WS-SUB)
                MOVE 200-TAX-ID
                           TO WS-TAX-ID-F2F3(WS-SUB)
                MOVE 200-MCC
                           TO WS-MCC-F2F3(WS-SUB)
                MOVE 200-CHAIN-MERCHANT-NUM
                           TO WS-CHAIN-MERCHANT-NUM-F2F3(WS-SUB)
                MOVE 200-MERCHANT-CONTACT
                           TO WS-MERCHANT-CONTACT-F2F3(WS-SUB)
                MOVE 200-MERCHANT-ACCOUNT
                           TO WS-MERCHANT-ACCOUNT-F2F3(WS-SUB)
                MOVE WSA-MERCHANT-ADDRESS-1(WS-ID-1)
                           TO WS-MERCHANT-ADDRESS-1-F2F3(WS-SUB)
                MOVE WSA-MERCHANT-ADDRESS-2(WS-ID-1)
                           TO WS-MERCHANT-ADDRESS-2-F2F3(WS-SUB)
                MOVE WSA-ZIP-CODE(WS-ID-1)
                           TO WS-ZIP-CODE-F2F3(WS-SUB)
           END-SEARCH.
           PERFORM 8100-READ-FILE-2        THRU 8100-EXIT.
       4500-EXIT.
           EXIT.
       5000-LOAD-ARRAY-FILE-4.
           MOVE ZERO                       TO WS-SUB.
           PERFORM VARYING WS-SUB          FROM +1 BY +1
               UNTIL FILE-AT-END           OF WS-400-FILE-STATUS
               OR WS-SUB >= WS-ARRAY-MAX-4
                  MOVE 400-INPUT-RECORD    TO WS-ARRAY-CUSTOMER(WS-SUB)
                  PERFORM 8300-READ-FILE-4 THRU 8300-EXIT
           END-PERFORM.

           IF WS-SUB > WS-ARRAY-MAX-3
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF.
           MOVE ZERO                       TO WS-SUB.
       5000-EXIT.
           EXIT.
       5500-MATCHING-CC.
           SET WS-ID-4 TO 1
           SEARCH WS-ARRAY-CUSTOMER
               WHEN WSA-CUSTOMER-ID(WS-ID-4) = 500-CUSTOMER-ID
                ADD 1                      TO WS-SUB
                MOVE WSA-CUSTOMER-ID(WS-ID-4)
                           TO WSA-CUSTOMER-ID-F4F5(WS-SUB)
                MOVE WSA-CUSTOMER-NAME(WS-ID-4)
                           TO WSA-CUSTOMER-NAME-F4F5(WS-SUB)
                MOVE WSA-PHONE-NUMBER(WS-ID-4)
                           TO WSA-PHONE-NUMBER-F4F5(WS-SUB)
                MOVE WSA-ADDRESS-1(WS-ID-4)
                           TO WSA-ADDRESS-1-F4F5(WS-SUB)
                MOVE WSA-ADDRESS-2(WS-ID-4)
                           TO WSA-ADDRESS-2-F4F5(WS-SUB)
                MOVE WSA-CUS-ZIP-CODE(WS-ID-4)
                           TO WSA-ZIP-CODE-F4F5(WS-SUB)
                MOVE 500-CARD-NUMBER
                           TO WSA-CARD-NUMBER-F4F5(WS-SUB)
                MOVE 500-CARD-TYPE
                           TO WSA-CARD-TYPE-F4F5(WS-SUB)
                MOVE 500-EXP-DATE
                           TO WSA-EXP-DATE-F4F5(WS-SUB)
                MOVE 500-SECURITY-CODE
                           TO WSA-SECURITY-CODE-F4F5(WS-SUB)
                MOVE 500-CREDIT-LIMIT
                           TO WSA-CREDIT-LIMIT-F4F5(WS-SUB)
           END-SEARCH.
           PERFORM 8400-READ-FILE-5        THRU 8400-EXIT.
       5500-EXIT.
           EXIT.
       6000-MATCHING-ALL.
           SET WS-ID-2 TO 1
           SET WS-ID-5 TO 1

           SEARCH WS-ARRAY-MATCHING-F2F3
               WHEN    WS-MERCHANT-NUMBER-F2F3(WS-ID-2) =
                       100-MERCHANT-NUMBER
                ADD 1      TO WS-SUB
                MOVE 100-CARD-NUMBER
                           TO WS-CARD-NUMBER-F1F2F3(WS-SUB)
                MOVE 100-MERCHANT-NUMBER
                           TO WS-MERCHANT-NUMBER-F1F2F3(WS-SUB)
                MOVE 100-AMOUNT
                           TO WS-AMOUNT-F1F2F3(WS-SUB)
                MOVE 100-MDR
                           TO WS-MDR-F1F2F3(WS-SUB)
                MOVE 100-VAT
                           TO WS-VAT-F1F2F3(WS-SUB)
                MOVE 100-CARD-TYPE
                           TO WS-CARD-TYPE-F1F2F3(WS-SUB)
                MOVE 100-TXN-DATE
                           TO WS-TXN-DATE-F1F2F3(WS-SUB)
                MOVE WS-PROFILE-F2F3(WS-ID-2)
                           TO WS-PROFILE-F1F2F3(WS-SUB)
                MOVE WS-MERCHANT-NAME-F2F3(WS-ID-2)
                           TO WS-MERCHANT-NAME-F1F2F3(WS-SUB)
                MOVE WS-TAX-ID-F2F3(WS-ID-2)
                           TO WS-TAX-ID-F1F2F3(WS-SUB)
                MOVE WS-MCC-F2F3(WS-ID-2)
                           TO WS-MCC-F1F2F3(WS-SUB)
                MOVE WS-CHAIN-MERCHANT-NUM-F2F3(WS-ID-2)
                           TO WS-CHAIN-MERCHANT-NUM-F1F2F3(WS-SUB)
                MOVE WS-MERCHANT-CONTACT-F2F3(WS-ID-2)
                           TO WS-MERCHANT-CONTACT-F1F2F3(WS-SUB)
                MOVE WS-MERCHANT-ACCOUNT-F2F3(WS-ID-2)
                           TO WS-MERCHANT-ACCOUNT-F1F2F3(WS-SUB)
                MOVE WS-MERCHANT-ADDRESS-1-F2F3(WS-ID-2)
                           TO WS-MERCHANT-ADDRESS-1-F1F2F3(WS-SUB)
                MOVE WS-MERCHANT-ADDRESS-2-F2F3(WS-ID-2)
                           TO WS-MERCHANT-ADDRESS-2-F1F2F3(WS-SUB)
                MOVE WS-ZIP-CODE-F2F3(WS-ID-2)
                           TO WS-ZIP-CODE-F1F2F3(WS-SUB)
           END-SEARCH.
      *************************** ERROR PUT INDEX **********************
           SEARCH WS-ARRAY-MATCHING-F4F5
               WHEN WSA-CARD-NUMBER-F4F5(WS-ID-5) =
                       100-CARD-NUMBER
                MOVE WSA-CUSTOMER-ID-F4F5(WS-ID-5)
                           TO WS-CUSTOMER-ID-F4F5(WS-SUB)
                MOVE WSA-CUSTOMER-NAME-F4F5(WS-ID-5)
                           TO WS-CUSTOMER-NAME-F4F5(WS-SUB)
                MOVE WSA-PHONE-NUMBER-F4F5(WS-ID-5)
                           TO WS-PHONE-NUMBER-F4F5(WS-SUB)
                MOVE WSA-ADDRESS-1-F4F5(WS-ID-5)
                           TO WS-ADDRESS-1-F4F5(WS-SUB)
                MOVE WSA-ADDRESS-2-F4F5(WS-ID-5)
                           TO WS-ADDRESS-2-F4F5(WS-SUB)
                MOVE WSA-ZIP-CODE-F4F5(WS-ID-5)
                           TO WS-ZIP-CODE-F4F5(WS-SUB)
                MOVE WSA-CARD-NUMBER-F4F5(WS-ID-5)
                           TO WS-CARD-NUMBER-F4F5(WS-SUB)
                MOVE WSA-CARD-TYPE-F4F5(WS-ID-5)
                           TO WS-CARD-TYPE-F4F5(WS-SUB)
                MOVE WSA-EXP-DATE-F4F5(WS-ID-5)
                           TO WS-EXP-DATE-F4F5(WS-SUB)
                MOVE WSA-SECURITY-CODE-F4F5(WS-ID-5)
                           TO WS-SECURITY-CODE-F4F5(WS-SUB)
                MOVE WSA-CREDIT-LIMIT-F4F5(WS-ID-5)
                           TO WS-CREDIT-LIMIT-F4F5(WS-SUB)

           END-SEARCH.

           PERFORM 8000-READ-FILE-1        THRU 8000-EXIT.
       6000-EXIT.
           EXIT.
       7000-WRITE-FILE-1.
           WRITE 700-MATCH-RECORD
           IF FILE-OK OF WS-700-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE-1
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 7 : " WS-700-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7000-EXIT.
           EXIT.
       7100-WRITE-FILE-2.
           WRITE 800-MATCH-RECORD
           IF FILE-OK OF WS-800-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE-2
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 8 : " WS-800-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7100-EXIT.
           EXIT.
       7200-WRITE-FILE-3.
           WRITE 900-MATCH-RECORD
           IF FILE-OK OF WS-900-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE-3
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 9 : " WS-900-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7200-EXIT.
           EXIT.
       8000-READ-FILE-1.
           READ 100-INPUT-FILE.
           IF FILE-OK                      OF WS-100-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-1
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-100-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "FILE STATUS 1 : " WS-100-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8000-EXIT.
           EXIT.
       8100-READ-FILE-2.
           READ 200-INPUT-FILE.
           IF FILE-OK                      OF WS-200-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-2
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-200-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "FILE STATUE 2 : " WS-200-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8100-EXIT.
           EXIT.
       8200-READ-FILE-3.
           READ 300-INPUT-FILE.
           IF FILE-OK                      OF WS-300-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-3
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-300-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "FILE STATUE 3 : " WS-300-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8200-EXIT.
           EXIT.
       8300-READ-FILE-4.
           READ 400-INPUT-FILE.
           IF FILE-OK                      OF WS-400-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-4
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-400-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "FILE STATUE 4 : " WS-400-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8300-EXIT.
           EXIT.
       8400-READ-FILE-5.
           READ 500-INPUT-FILE.
           IF FILE-OK                      OF WS-500-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-5
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-500-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "FILE STATUE 5 : " WS-500-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8400-EXIT.
           EXIT.
       9000-ABEND.
           MOVE 12 TO RETURN-CODE.
           GOBACK.
       9000-EXIT.
           EXIT.
