       IDENTIFICATION DIVISION.
       PROGRAM-ID. REPORT-CUSTOMER.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      ********************* INPUT OUTPUT FILE **************************
           SELECT 100-INPUT-FILE ASSIGN
               TO "C:../Requirement/MATCHING-FILE.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-100-FILE-STATUS.
           SELECT 200-OUTPUT-FILE ASSIGN
               TO "C:../Requirement/CustomerStatement1.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-200-FILE-STATUS.
           SELECT 300-OUTPUT-FILE ASSIGN
               TO "C:../Requirement/CustomerStatement2.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-300-FILE-STATUS.
           SELECT 400-OUTPUT-FILE ASSIGN
               TO "C:../Requirement/CustomerStatement3.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-400-FILE-STATUS.
      ********************* INPUT OUTPUT FILE **************************
       DATA DIVISION.
       FILE SECTION.
      *************************    FD      *****************************
       FD 100-INPUT-FILE.
       01 100-INPUT-RECORD.
           05 100-CARD-NUMBER-TYPE-INC     PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-NUMBER-INC      PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-AMOUNT-INC               PIC 9(7)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MDR-INC                  PIC 9(6)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-VAT-INC                  PIC 9(5)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-TYPE-INC            PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-TXN-DATE-INC             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-PROFILE-MERC             PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-NAME            PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-TAX-ID-MERC              PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MCC-MERC                 PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CHAIN-MERCHANT-NUM       PIC X(21)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ACCOUNT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ADDRESS-1       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-ZIP-CODE-MERC            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 100-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-PHONE-NUMBER    PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ADDRESS-1       PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ADDRESS-2       PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ZIP-CODE        PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 100-CARD-NUMBER              PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-TYPE                PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-EXP-DATE            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-SECURITY-CODE       PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-CREDIT-LIMIT        PIC 9(8)V9(2)   VALUE ZERO.

       FD 200-OUTPUT-FILE.
       01 200-OUTPUT-RECORD                PIC X(133).
       FD 300-OUTPUT-FILE.
       01 300-OUTPUT-RECORD                PIC X(133).
       FD 400-OUTPUT-FILE.
       01 400-OUTPUT-RECORD                PIC X(133).
      *************************    FD      *****************************

       WORKING-STORAGE SECTION.
       01 WS-FILE-STATUS.
           05  WS-100-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-200-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-300-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-400-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-500-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
      *************************   CALCULATORs **************************
       01 WS-CAL.
           05 WS-REC-COUNT-FILE-1          PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE1              PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE2              PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE3              PIC 9(4)        VALUE ZERO.
           05 WS-SUB                       PIC 9(4)        VALUE ZERO.
           05 WS-SUB-1                     PIC 9(4)        VALUE ZERO.
           05 WS-SUB-2                     PIC 9(4)        VALUE ZERO.
           05 WS-SUB-3                     PIC 9(4)        VALUE ZERO.

           05 WS-POINTER                   PIC 9(4)        VALUE ZERO.

           05 WS-ARRAY-MAX                 PIC 9(4)        VALUE 17.
           05 WS-ARRAY-MAX-1               PIC 9(4)        VALUE 17.

           05 WS-LINE-MAX-FILE1            PIC S9(2)       VALUE +55.
           05 WS-LINE-HEADER-FILE1         PIC S9(3)       VALUE +12.
           05 WS-LINE-COUNT-FILE1          PIC S9(2)       VALUE +55.

           05 WS-LINE-MAX-FILE2            PIC S9(2)       VALUE +55.
           05 WS-LINE-HEADER-FILE2         PIC S9(3)       VALUE +12.
           05 WS-LINE-COUNT-FILE2          PIC S9(2)       VALUE +55.

           05 WS-LINE-MAX-FILE3            PIC S9(2)       VALUE +55.
           05 WS-LINE-HEADER-FILE3         PIC S9(3)       VALUE +12.
           05 WS-LINE-COUNT-FILE3          PIC S9(2)       VALUE +55.

           05 WS-TXN-AMOUNT                PIC 9(7)V9(2)   VALUE ZERO.
           05 WS-GRAND-AMOUNT              PIC 9(8)V9(2)   VALUE ZERO.

           05 WS-MIN-PAYMENT               PIC 9(8)V9(2)   VALUE ZERO.

           05 WS-COUNT-DETAIL-CARD         PIC 9           VALUE ZERO.
           05 WS-COUNT-TYPE-CARD           PIC 9           VALUE ZERO.
           05 WS-INDEX                     PIC 9(4)        VALUE ZERO.
      *************************   CALCULATORs **************************

      *************************   DATE TIME   **************************
       01 WS-DATE-NOW.
           05 YEAR-NOW                     PIC X(4)        VALUE SPACE.
           05 MONTH-NOW                    PIC X(2)        VALUE SPACE.
           05 DAY-NOW                      PIC X(2)        VALUE SPACE.
       01 WS-TIME-NOW.
           05 HOURS-NOW                    PIC X(2)        VALUE SPACE.
           05 MINUTE-NOW                   PIC X(2)        VALUE SPACE.
           05 SECOND-NOW                   PIC X(2)        VALUE SPACE.
      *************************   DATE TIME   **************************

      ************************* HEADER LINE 1 **************************
       01 WS-HEADER-1.
            05 FILLER                      PIC X(45)
               VALUE "REPORT-NO: CR06 ".
            05 FILLER                      PIC X(25)
               VALUE "CUSTOMER STATEMENT REPORT".
            05 FILLER                      PIC X(45)
               VALUE SPACE.
            05 FILLER                      PIC X(7)
               VALUE "DATE : ".
            05 DDAY                        PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE "/".
            05 MONTH                       PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE "/".
            05 YEAR                        PIC X(4).

      ************************* HEADER LINE 1 **************************

      ************************* HEADER LINE 2 **************************
       01 WS-HEADER-2.
            05 FILLER                      PIC X(48)
               VALUE "PROGRAM  : TRR0006".
            05 FILLER                      PIC X(67)
               VALUE SPACE.
            05 FILLER                      PIC X(7)
               VALUE "TIME : ".
            05 HOUR                        PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE ":".
            05 MINUTE                      PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE ":".
            05 SECOND                      PIC X(2).
      ************************* HEADER LINE 2 **************************

      ************************* HEADER DETAIL **************************
       01  WS-HEADER-DETAIL.
           05 WS-ACCOUNT-SUMMARY           PIC X(15)
              VALUE "ACCOUNT SUMMARY".
           05 WS-HEADER-ACCOUNT-SUMMARY.
              10 FILLER                    PIC X(5)        VALUE SPACE.
              10 FILLER                    PIC X(26)
                 VALUE "CARD NUMBER".
              10 FILLER                    PIC X(29)
                 VALUE "ACCOUNT NAME".
              10 FILLER                    PIC X(31)
                 VALUE "CREDIT LIMIT".
              10 FILLER                    PIC X(30)
                 VALUE "BALANCE".
              10 FILLER                    PIC X(11)
                 VALUE "MIN PAYMENT".
           05 WS-CHECK-DETAIL-LINE         PIC X           VALUE "N".
              88 WS-IS-CHANGE-CARD         VALUE "Y".
              88 WS-NOT-IS-CHANGE-CARD     VALUE "N".

           05 WS-ACCOUNT-DETAIL            PIC X(14)
              VALUE "ACCOUNT DETAIL".
           05 WS-HEADER-ACCOUNT-DETAIL.
              10 FILLER                    PIC X(7)        VALUE SPACE.
              10 FILLER                    PIC X(46)
                 VALUE "CARD TYPE".
              10 FILLER                    PIC X(53)
                 VALUE "CARD NUMBER".
              10 FILLER                    PIC X(26)
                 VALUE "ACCOUNT NAME".
           05 WS-HEADER-DESCRIPTION.
              10 FILLER                    PIC X(27)
                 VALUE "TRAN DATE".
              10 FILLER                    PIC X(46)
                 VALUE "POSTING DATE".
              10 FILLER                    PIC X(44)
                 VALUE "DESCRPTION".
              10 FILLER                    PIC X(15)
                 VALUE "AMOUNT  (BAHT)".
          05 WS-TOTAL-LINE.
              10 FILLER                    PIC X(49)       VALUE SPACE.
              10 FILLER                    PIC X(55)
                 VALUE "***** TOTAL BALANCE *****".
              10 WS-TOTAL-BALANCE          PIC ZZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
      ************************* HEADER DETAIL **************************

      ************************* FORMAT NUMBER **************************
       01 WS-FORMAT-PHONE-NO.
           05 WS-PHONE-POS1                PIC X(3)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-PHONE-POS2                PIC X(3)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-PHONE-POS3                PIC X(4)        VALUE SPACE.
       01 WS-FORMAT-CARD-ID.
           05 WS-CARD-POS1                 PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-CARD-POS2                 PIC X(2)        VALUE SPACE.
           05 FILLER                       PIC X(8)    VALUE "XX-XXXX-".
           05 WS-CARD-POS3                 PIC X(4)        VALUE SPACE.
      ************************* FORMAT NUMBER **************************

      *************************     ARRAY     **************************
       01 WS1-ARRAY-ALL-FILE               OCCURS 20
                                           ascending WS1-CUSTOMER-ID
                                           descending WS1-CARD-NUMBER.
           05 WS1-CARD-NUMBER-TYPE-INC     PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-NUMBER-INC      PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-AMOUNT-INC               PIC 9(7)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MDR-INC                  PIC 9(6)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-VAT-INC                  PIC 9(5)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-TYPE-INC            PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-TXN-DATE-INC             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-PROFILE-MERC             PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-NAME            PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-TAX-ID-MERC              PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MCC-MERC                 PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CHAIN-MERCHANT-NUM       PIC X(21)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-ACCOUNT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-ADDRESS-1       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-ZIP-CODE-MERC            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-PHONE-NUMBER    PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-ADDRESS-1       PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-ADDRESS-2       PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CUSTOMER-ZIP-CODE        PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-NUMBER              PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-TYPE                PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-EXP-DATE            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-SECURITY-CODE       PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS1-CARD-CREDIT-LIMIT        PIC 9(8)V9(2)   VALUE ZERO.

       01 WS2-ARRAY-ALL-FILE               OCCURS 50 INDEXED BY WS-ID-2.
           05 WS2-CARD-TYPE                PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-CARD-NUMBER              PIC X(19)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-TXN-DATE-INC             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-POST-DATE                PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-MERCHANT-NAME            PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS2-AMOUNT-INC               PIC 9(7)V9(2)   VALUE ZERO.

      *************************     ARRAY     **************************


       01 WS-CHECK-KEY.
           05 WS-CONTROL-KEY1               PIC X(15)      VALUE SPACE.
           05 WS-CONTROL-KEY2               PIC X(16)      VALUE SPACE.

           05 WS-CERRENT-KEY                PIC 9          VALUE ZERO.

      ************************* OUTPUT DETAIL **************************
       01 WS-OUTPUT-LINE-1.
           05 WS-OUTPUT-CARD-NUM           PIC X(19)       VALUE SPACE.
           05 FILLER                       PIC X(10)       VALUE " | ".
           05 WS-OUTPUT-CUSTOMER-NAME      PIC X(16)       VALUE SPACE.
           05 FILLER                       pIC X(10)       VALUE " | ".
           05 WS-OUTPUT-CREDIT-LIMIT       PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
           05 FILLER                       pIC X(10)       VALUE " | ".
           05 WS-OUTPUT-BALANCE            PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
           05 FILLER                       pIC X(10)       VALUE " | ".
           05 WS-OUTPUT-MIN-PAYMENT        PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
           05 FILLER                       pIC X(10)       VALUE " | ".

      ************************* OUTPUT DETAIL **************************

       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITIAL            THRU 1000-EXIT.
           PERFORM 2000-PROCESS            THRU 2000-EXIT.
           PERFORM 3000-END                THRU 3000-EXIT.
           GOBACK.
       1000-INITIAL.
           OPEN INPUT  100-INPUT-FILE
                OUTPUT 200-OUTPUT-FILE
                       300-OUTPUT-FILE
                       400-OUTPUT-FILE.
           PERFORM 1100-INITIAL-DATE-TIME  THRU 1100-EXIT.

           PERFORM 8000-READ-FILE-1        THRU 8000-EXIT.
           PERFORM 3500-LOAD-ARRAY         THRU 3500-EXIT.
       1000-EXIT.
           EXIT.
       1100-INITIAL-DATE-TIME.
           ACCEPT WS-DATE-NOW              FROM DATE YYYYMMDD.
           ACCEPT WS-TIME-NOW              FROM TIME.

           MOVE DAY-NOW                    TO DDAY.
           MOVE MONTH-NOW                  TO MONTH.
           MOVE YEAR-NOW                   TO YEAR.

           MOVE HOURS-NOW                  TO HOUR.
           MOVE MINUTE-NOW                 TO MINUTE.
           MOVE SECOND-NOW                 TO SECOND.
       1100-EXIT.
           EXIT.
       2000-PROCESS.
           MOVE 1                          TO WS-SUB
                                              WS-SUB-1
                                              WS-POINTER
                                              WS-CERRENT-KEY.

           MOVE WS1-CUSTOMER-ID(WS-SUB)    TO WS-CONTROL-KEY1.
           MOVE WS1-CARD-NUMBER(WS-SUB)    TO WS-CONTROL-KEY2.

           PERFORM VARYING WS-SUB          FROM +1 BY +1
               UNTIL WS-SUB >= WS-ARRAY-MAX
                   PERFORM 6500-FORMAT-CARD-ID     THRU 6500-EXIT
                   PERFORM 4500-PROCESS    THRU 4500-EXIT
                   ADD +1 TO WS-SUB-1
                   MOVE WS-SUB
                           TO WS-OUTPUT-CUSTOMER-NAME
                   MOVE WS-FORMAT-CARD-ID
                           TO WS-OUTPUT-CARD-NUM
                   MOVE WS-GRAND-AMOUNT
                           TO WS-OUTPUT-BALANCE
                   MOVE WS1-CARD-CREDIT-LIMIT(WS-SUB)
                           TO WS-OUTPUT-CREDIT-LIMIT
                   COMPUTE WS-MIN-PAYMENT = (WS-GRAND-AMOUNT * 10) / 100
                   MOVE WS-MIN-PAYMENT TO WS-OUTPUT-MIN-PAYMENT
           END-PERFORM.

       2000-EXIT.
           EXIT.
       3000-END.
           DISPLAY "###################################################"
           PERFORM VARYING WS-SUB FROM  +1 BY +1
               UNTIL WS-SUB > WS-ARRAY-MAX-1
               DISPLAY WS-SUB " : " WS2-ARRAY-ALL-FILE(WS-SUB)
           END-PERFORM.
           DISPLAY "###################################################"

           DISPLAY "INPUT  FILE 1 : " WS-REC-COUNT-FILE-1.
           DISPLAY "OUTPUT FILE 2 : " WS-OUTPUT-FILE1.
           DISPLAY "OUTPUT FILE 3 : " WS-OUTPUT-FILE2.
           DISPLAY "OUTPUT FILE 4 : " WS-OUTPUT-FILE3.

           DISPLAY "LINE COUNT CUSTOMER 3 : " WS-LINE-COUNT-FILE1
           DISPLAY "LINE COUNT CUSTOMER 2 : " WS-LINE-COUNT-FILE2
           DISPLAY "LINE COUNT CUSTOMER 1 : " WS-LINE-COUNT-FILE3
           CLOSE 100-INPUT-FILE
                 200-OUTPUT-FILE
                 300-OUTPUT-FILE
                 400-OUTPUT-FILE.
       3000-EXIT.
           EXIT.
       3500-LOAD-ARRAY.
           MOVE ZERO                       TO WS-SUB.
           PERFORM VARYING WS-SUB          FROM +1 BY +1
               UNTIL FILE-AT-END           OF WS-100-FILE-STATUS
               OR WS-SUB >= WS-ARRAY-MAX
                   MOVE 100-INPUT-RECORD   TO WS1-ARRAY-ALL-FILE(WS-SUB)
               PERFORM 8000-READ-FILE-1    THRU 8000-EXIT
           END-PERFORM.
           IF WS-SUB > WS-ARRAY-MAX
              DISPLAY "DEBUGGING"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF.
           MOVE ZERO                       TO WS-SUB.
       3500-EXIT.
           EXIT.

       4000-INITIAL.

       4000-EXIT.
           EXIT.
       4500-PROCESS.

           IF WS-CONTROL-KEY1 = WS1-CUSTOMER-ID(WS-SUB)
               IF WS-CONTROL-KEY2 = WS1-CARD-NUMBER(WS-SUB)

                   PERFORM 4900-TOTAL-LINE         THRU 4900-EXIT
                   SET WS-NOT-IS-CHANGE-CARD       TO TRUE
                   ADD WS1-AMOUNT-INC(WS-SUB)      TO WS-TXN-AMOUNT
                                                      WS-GRAND-AMOUNT
           DISPLAY "======================================== SUB : "
           WS-SUB " SAME "
               ELSE
                  DISPLAY " CHAINING CARD "
                  ADD +1 TO WS-COUNT-TYPE-CARD
                  PERFORM 4900-TOTAL-LINE          THRU 4900-EXIT
                  MOVE WS-TXN-AMOUNT               TO WS-MIN-PAYMENT
                   DISPLAY "SUB : " WS-SUB " "WS-INDEX" "
                   WS-COUNT-DETAIL-CARD
                  ADD WS-COUNT-DETAIL-CARD         TO WS-INDEX
                  SET WS-COUNT-DETAIL-CARD         TO 0
                  DISPLAY " CHAINING CARD INDEX : " WS-COUNT-DETAIL-CARD
                           " "WS-INDEX
                  SET WS-NOT-IS-CHANGE-CARD        TO TRUE
                  PERFORM 4700-CHECK-CUS           THRU 4700-EXIT

                  MOVE ZERO                        TO WS-TXN-AMOUNT
                                                      WS-GRAND-AMOUNT


                  MOVE WS1-CARD-NUMBER(WS-SUB)     TO WS-CONTROL-KEY2
                  ADD WS1-AMOUNT-INC(WS-SUB)       TO WS-TXN-AMOUNT
                                                      WS-GRAND-AMOUNT
               END-IF
           DISPLAY "======================================== SUB : "
           WS-SUB " CHAINING "
           ELSE
              DISPLAY " CHAINING CUTOMER "
               ADD +1 TO WS-COUNT-TYPE-CARD

               PERFORM 4700-CHECK-CUS              THRU 4700-EXIT
               SET WS-IS-CHANGE-CARD               TO TRUE
               PERFORM 4900-TOTAL-LINE             THRU 4900-EXIT

               PERFORM 4700-CHECK-CUS              THRU 4700-EXIT
               SET WS-NOT-IS-CHANGE-CARD           TO TRUE
               MOVE ZERO                           TO WS-TXN-AMOUNT
                                                      WS-GRAND-AMOUNT
               MOVE WS1-CUSTOMER-ID(WS-SUB)        TO WS-CONTROL-KEY1
               MOVE WS1-CARD-NUMBER(WS-SUB)        TO WS-CONTROL-KEY2
               ADD WS1-AMOUNT-INC(WS-SUB)          TO WS-TXN-AMOUNT
                                                      WS-GRAND-AMOUNT
               SET WS-COUNT-TYPE-CARD              TO 0
               SET WS-COUNT-DETAIL-CARD            TO 0
               SET WS-NOT-IS-CHANGE-CARD           TO TRUE
               ADD +1 TO WS-CERRENT-KEY
           END-IF.
           ADD +1 TO WS-COUNT-DETAIL-CARD.
       4500-EXIT.
           EXIT.
       4700-CHECK-CUS.
           EVALUATE WS-CERRENT-KEY         ALSO    TRUE
               WHEN 1                      ALSO WS-NOT-IS-CHANGE-CARD
                   PERFORM 6000-CHACK-HEADER-FILE1 THRU 6000-EXIT

                   MOVE WS-OUTPUT-LINE-1           TO 200-OUTPUT-RECORD
                   PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
               WHEN 1                      ALSO WS-IS-CHANGE-CARD

               PERFORM VARYING WS-SUB-2 FROM +1 BY +1
                       UNTIL WS-SUB-2 > WS-COUNT-TYPE-CARD
                       PERFORM 6100-CHECK-HEADER-DETAIL-FILE1
                                           THRU 6100-EXIT
               END-PERFORM
      ******************************************************************
               WHEN 2                       ALSO WS-NOT-IS-CHANGE-CARD
                   PERFORM 6200-CHACK-HEADER-FILE2 THRU 6200-EXIT
                   MOVE WS-OUTPUT-LINE-1           TO 300-OUTPUT-RECORD
                   PERFORM 7200-WRITE-FILE-2       THRU 7200-EXIT

               WHEN 2                      ALSO WS-IS-CHANGE-CARD

               PERFORM VARYING WS-SUB-2 FROM +1 BY +1
                       UNTIL WS-SUB-2 > WS-COUNT-TYPE-CARD
                       PERFORM 6300-CHECK-HEADER-DETAIL-FILE2
                                           THRU 6300-EXIT
               END-PERFORM
      ******************************************************************
               WHEN 3                      ALSO WS-NOT-IS-CHANGE-CARD
                   PERFORM 6400-CHACK-HEADER-FILE3 THRU 6400-EXIT
                   MOVE WS-OUTPUT-LINE-1           TO 400-OUTPUT-RECORD
                   PERFORM 7300-WRITE-FILE-3       THRU 7300-EXIT
               WHEN 3                      ALSO WS-IS-CHANGE-CARD

               PERFORM VARYING WS-SUB-2 FROM +1 BY +1
                       UNTIL WS-SUB-2 > WS-COUNT-TYPE-CARD
                       PERFORM 6450-CHECK-HEADER-DETAIL-FILE3
                                           THRU 6450-EXIT
               END-PERFORM
      ******************************************************************
           END-EVALUATE.
       4700-EXIT.
           EXIT.
       4900-TOTAL-LINE.

           MOVE WS1-CARD-TYPE(WS-SUB)  TO WS2-CARD-TYPE(WS-SUB-1).
           MOVE WS1-CUSTOMER-NAME(WS-SUB)
                                       TO WS2-CUSTOMER-NAME(WS-SUB-1).
           MOVE WS-FORMAT-CARD-ID      TO WS2-CARD-NUMBER(WS-SUB-1).
           MOVE WS1-TXN-DATE-INC(WS-SUB)
                                       TO WS2-TXN-DATE-INC(WS-SUB-1).
           MOVE WS-DATE-NOW            TO WS2-POST-DATE(WS-SUB-1).
           MOVE WS1-MERCHANT-NAME(WS-SUB)
                                       TO WS2-MERCHANT-NAME(WS-SUB-1).
           MOVE WS1-MERCHANT-ADDRESS-2(WS-SUB)
                                  TO WS2-MERCHANT-ADDRESS-2(WS-SUB-1).
           MOVE WS1-AMOUNT-INC(WS-SUB) TO WS2-AMOUNT-INC(WS-SUB-1).

           MOVE WS-GRAND-AMOUNT        TO WS-TOTAL-BALANCE.
       4900-EXIT.
           EXIT.

       6000-CHACK-HEADER-FILE1.
           ADD +1                          TO WS-LINE-COUNT-FILE1.
           IF WS-LINE-COUNT-FILE1 >= WS-LINE-MAX-FILE1
               MOVE WS-HEADER-1            TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE WS-HEADER-2            TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT

               MOVE WS1-CUSTOMER-NAME(WS-SUB)
                                           TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT

               PERFORM 6900-FORMATE-CUS-PHONE-NO THRU 6900-EXIT
               MOVE WS-FORMAT-PHONE-NO         TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT

               PERFORM 6600-FORMAT-STRING  THRU 6600-EXIT
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT

               MOVE WS1-CUSTOMER-ZIP-CODE(WS-SUB)  TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE ALL SPACE              TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE WS-ACCOUNT-SUMMARY     TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE ALL "-"                TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE WS-HEADER-ACCOUNT-SUMMARY
                                           TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT
               MOVE ALL "-"                TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1   THRU 7100-EXIT

               MOVE WS-LINE-HEADER-FILE1   TO WS-LINE-COUNT-FILE1
           END-IF.
      ******************************************************************
       6000-EXIT.
           EXIT.

       6100-CHECK-HEADER-DETAIL-FILE1.
           DISPLAY "FILE 1 <==========================================="
           DISPLAY "WS-SUB-3               : " WS-SUB-3
           DISPLAY "WS-POINTER             : "WS-POINTER
           DISPLAY "WS-INDEX               : "WS-INDEX
           DISPLAY "WS-COUNT-DETAIL-CARD   : " WS-COUNT-DETAIL-CARD

           ADD +6                          TO WS-LINE-COUNT-FILE1.
           MOVE ALL SPACE                  TO 200-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
           MOVE WS-ACCOUNT-DETAIL          TO 200-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
           MOVE ALL "-"                    TO 200-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
           MOVE WS-HEADER-ACCOUNT-DETAIL   TO 200-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
           MOVE ALL "-"                    TO 200-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT.

           PERFORM VARYING WS-SUB-3 FROM WS-POINTER BY +1
               UNTIL WS-SUB-3 > WS-INDEX
               MOVE WS2-ARRAY-ALL-FILE(WS-SUB-3)  TO 200-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT
           END-PERFORM.
               MOVE WS-TOTAL-LINE              TO 200-OUTPUT-RECORD.
               PERFORM 7100-WRITE-FILE-1       THRU 7100-EXIT.
           MOVE WS-SUB-3 TO WS-POINTER.
           ADD WS-COUNT-DETAIL-CARD TO WS-INDEX.
       6100-EXIT.
           EXIT.

       6200-CHACK-HEADER-FILE2.
           ADD +1                          TO WS-LINE-COUNT-FILE2.
           IF WS-LINE-COUNT-FILE2 >= WS-LINE-MAX-FILE2
               MOVE WS-HEADER-1            TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE WS-HEADER-2            TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT

               MOVE  WS1-CUSTOMER-NAME(WS-SUB)     TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT

               PERFORM 6900-FORMATE-CUS-PHONE-NO THRU 6900-EXIT
               MOVE WS-FORMAT-PHONE-NO         TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2       THRU 7200-EXIT

               PERFORM 6700-FORMAT-STRING  THRU 6700-EXIT
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE WS1-CUSTOMER-ZIP-CODE(WS-SUB)  TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE ALL SPACE              TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT

               MOVE WS-ACCOUNT-SUMMARY     TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE ALL "-"                TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE WS-HEADER-ACCOUNT-SUMMARY
                                           TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE ALL "-"                TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2   THRU 7200-EXIT
               MOVE WS-LINE-HEADER-FILE2   TO WS-LINE-COUNT-FILE2

           END-IF.
       6200-EXIT.
           EXIT.
       6300-CHECK-HEADER-DETAIL-FILE2.
           DISPLAY "FILE 2 <==========================================="
           DISPLAY "WS-SUB-3               : " WS-SUB-3
           DISPLAY "WS-POINTER             : "WS-POINTER
           DISPLAY "WS-INDEX               : "WS-INDEX
           DISPLAY "WS-COUNT-DETAIL-CARD   : " WS-COUNT-DETAIL-CARD

           ADD +6                          TO WS-LINE-COUNT-FILE2.
           MOVE ALL SPACE                  TO      300-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-2       THRU    7200-EXIT
           MOVE WS-ACCOUNT-DETAIL          TO      300-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-2       THRU    7200-EXIT
           MOVE ALL "-"                    TO      300-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-2       THRU    7200-EXIT
           MOVE WS-HEADER-ACCOUNT-DETAIL   TO      300-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-2       THRU    7200-EXIT
           MOVE ALL "-"                    TO      300-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-2       THRU    7200-EXIT.

           PERFORM VARYING WS-SUB-3 FROM WS-POINTER BY +1
               UNTIL WS-SUB-3 > WS-INDEX
               MOVE WS2-ARRAY-ALL-FILE(WS-SUB-3)  TO 300-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-2       THRU 7200-EXIT
           END-PERFORM.
               MOVE WS-TOTAL-LINE              TO 300-OUTPUT-RECORD.
               PERFORM 7200-WRITE-FILE-2       THRU 7200-EXIT.

           MOVE WS-SUB-3 TO WS-POINTER.

           DISPLAY "=========================== "WS-INDEX " TESTING ".
       6300-EXIT.
           EXIT.
       6400-CHACK-HEADER-FILE3.
           ADD +1                          TO WS-LINE-COUNT-FILE3.
           IF WS-LINE-COUNT-FILE3 >= WS-LINE-MAX-FILE3
               MOVE WS-HEADER-1            TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE WS-HEADER-2            TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT

               MOVE WS1-CUSTOMER-NAME(WS-SUB)   TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT

               PERFORM 6900-FORMATE-CUS-PHONE-NO THRU 6900-EXIT
               MOVE WS-FORMAT-PHONE-NO         TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3       THRU 7300-EXIT

               PERFORM 6800-FORMAT-STRING  THRU 6800-EXIT
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE WS1-CUSTOMER-ZIP-CODE(WS-SUB)  TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE ALL SPACE              TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT

               MOVE WS-ACCOUNT-SUMMARY     TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE ALL "-"                TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE WS-HEADER-ACCOUNT-SUMMARY
                                           TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT
               MOVE ALL "-"                TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3   THRU 7300-EXIT

               MOVE WS-LINE-HEADER-FILE3   TO WS-LINE-COUNT-FILE3
           END-IF.
       6400-EXIT.
           EXIT.
       6450-CHECK-HEADER-DETAIL-FILE3.
           DISPLAY "FILE 3 <==========================================="
           DISPLAY "WS-SUB-3               : " WS-SUB-3
           DISPLAY "WS-POINTER             : "WS-POINTER
           DISPLAY "WS-INDEX               : "WS-INDEX
           DISPLAY "WS-COUNT-DETAIL-CARD   : " WS-COUNT-DETAIL-CARD
           ADD +6                           TO WS-LINE-COUNT-FILE3.
           MOVE    ALL SPACE                TO      400-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-3        THRU    7300-EXIT
           MOVE    WS-ACCOUNT-DETAIL        TO      400-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-3        THRU    7300-EXIT
           MOVE    ALL "-"                  TO      400-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-3        THRU    7300-EXIT
           MOVE    WS-HEADER-ACCOUNT-DETAIL TO      400-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-3        THRU    7300-EXIT
           MOVE    ALL "-"                  TO      400-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-3        THRU    7300-EXIT.

           PERFORM VARYING WS-SUB-3 FROM WS-POINTER BY +1
               UNTIL WS-SUB-3 > WS-INDEX
               MOVE WS2-ARRAY-ALL-FILE(WS-SUB-3)  TO 400-OUTPUT-RECORD
               PERFORM 7300-WRITE-FILE-3       THRU 7300-EXIT
           END-PERFORM.
               MOVE WS-TOTAL-LINE              TO 400-OUTPUT-RECORD.
               PERFORM 7300-WRITE-FILE-3       THRU 7300-EXIT.

       6450-EXIT.
           EXIT.

       6500-FORMAT-CARD-ID.
           MOVE WS1-CARD-NUMBER(WS-SUB)(1:4)       TO WS-CARD-POS1.
           MOVE WS1-CARD-NUMBER(WS-SUB)(5:2)       TO WS-CARD-POS2.
           MOVE WS1-CARD-NUMBER(WS-SUB)(13:4)      TO WS-CARD-POS3.
       6500-EXIT.
           EXIT.

       6600-FORMAT-STRING.
           STRING  WS1-CUSTOMER-ADDRESS-1(WS-SUB) DELIMITED BY SIZE
                   WS1-CUSTOMER-ADDRESS-2(WS-SUB) DELIMITED BY SIZE
                   INTO 200-OUTPUT-RECORD
           END-STRING.
       6600-EXIT.
           EXIT.
       6700-FORMAT-STRING.
           STRING  WS1-CUSTOMER-ADDRESS-1(WS-SUB) DELIMITED BY SIZE
                   WS1-CUSTOMER-ADDRESS-2(WS-SUB) DELIMITED BY SIZE
                   INTO 300-OUTPUT-RECORD
           END-STRING.
       6700-EXIT.
           EXIT.
       6800-FORMAT-STRING.
           STRING  WS1-CUSTOMER-ADDRESS-1(WS-SUB) DELIMITED BY SIZE
                   WS1-CUSTOMER-ADDRESS-2(WS-SUB) DELIMITED BY SIZE
                   INTO 400-OUTPUT-RECORD
           END-STRING.
       6800-EXIT.
           EXIT.

       6900-FORMATE-CUS-PHONE-NO.
           MOVE WS1-CUSTOMER-PHONE-NUMBER(WS-SUB)(1:3)
                                           TO WS-PHONE-POS1.
           MOVE WS1-CUSTOMER-PHONE-NUMBER(WS-SUB)(4:3)
                                           TO WS-PHONE-POS2.
           MOVE WS1-CUSTOMER-PHONE-NUMBER(WS-SUB)(7:4)
                                           TO WS-PHONE-POS3.
       6900-EXIT.
           EXIT.




       7100-WRITE-FILE-1.
           WRITE 200-OUTPUT-RECORD.
           IF FILE-OK OF WS-200-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE1
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 2 : " WS-200-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7100-EXIT.
           EXIT.
       7200-WRITE-FILE-2.
           WRITE 300-OUTPUT-RECORD.
           IF FILE-OK OF WS-300-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE2
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 3 : " WS-300-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7200-EXIT.
           EXIT.
       7300-WRITE-FILE-3.
           WRITE 400-OUTPUT-RECORD.
           IF FILE-OK OF WS-400-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE3
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 4 : " WS-400-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7300-EXIT.
           EXIT.
       8000-READ-FILE-1.
           READ 100-INPUT-FILE.
           IF FILE-OK                      OF WS-100-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-1
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-100-FILE-STATUS
                       CONTINUE
               ELSE
                   DISPLAY "FILE STATUS 1 : " WS-100-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8000-EXIT.
           EXIT.
       9000-ABEND.
           MOVE 12 TO RETURN-CODE.
           GOBACK.
       9000-EXIT.
           EXIT.
