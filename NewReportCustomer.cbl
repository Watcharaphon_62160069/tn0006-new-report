       IDENTIFICATION DIVISION.
       PROGRAM-ID. REPORT-CUSTOMER.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      ********************* INPUT OUTPUT FILE **************************

           SELECT 100-INPUT-FILE ASSIGN TO WS-NAME-OF-FILE
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-100-FILE-STATUS.

           SELECT 400-OUTPUT-FILE ASSIGN
               TO WS-OUTPUT-FILE-NAME
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-400-FILE-STATUS.

      ********************* INPUT OUTPUT FILE **************************
       DATA DIVISION.
       FILE SECTION.
      *************************    FD      *****************************
       FD 100-INPUT-FILE.
       01 100-INPUT-RECORD.
           05 100-CARD-NUMBER-TYPE-INC     PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-NUMBER-INC      PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-AMOUNT-INC               PIC 9(7)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MDR-INC                  PIC 9(6)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-VAT-INC                  PIC 9(5)V9(2)   VALUE ZERO.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-TYPE-INC            PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-TXN-DATE-INC             PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-PROFILE-MERC             PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-NAME            PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-TAX-ID-MERC              PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MCC-MERC                 PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CHAIN-MERCHANT-NUM       PIC X(21)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-CONTACT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ACCOUNT         PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ADDRESS-1       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-MERCHANT-ADDRESS-2       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-ZIP-CODE-MERC            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 100-CUSTOMER-ID              PIC X(15)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-NAME            PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-PHONE-NUMBER    PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ADDRESS-1       PIC X(12)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ADDRESS-2       PIC X(10)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CUSTOMER-ZIP-CODE        PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.

           05 100-CARD-NUMBER              PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-TYPE                PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-EXP-DATE            PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-SECURITY-CODE       PIC X(13)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 100-CARD-CREDIT-LIMIT        PIC 9(8)V9(2)   VALUE ZERO.

       FD 400-OUTPUT-FILE.
       01 400-OUTPUT-RECORD                PIC X(133).

      *************************    FD      *****************************

       WORKING-STORAGE SECTION.
       01 WS-NAME-OF-FILE                  PIC X(39)   VALUE HIGH-VALUE.
           88 WS-CUSTOMER-1                VALUE
               "Requirement\CustomerStatement1.txt".
           88 WS-CUSTOMER-2                VALUE
               "Requirement/CustomerStatement2.txt".
           88 WS-CUSTOMER-3                VALUE
               "Requirement/CustomerStatement3.txt".
       01 WS-STATUS-INPUT                  PIC X       VALUE HIGH-VALUE.
           88 WS-INPUT-IS-TRUE             VALUE "Y".
           88 WS-INPUT-IS-FALSE            VALUE "N".
       01 WS-OUTPUT-FILE-NAME              PIC X(39)   VALUE HIGH-VALUE.
           88 WS-OUTPUT-CUS-1               VALUE
              "Requirement/Statement1.txt".
           88 WS-OUTPUT-CUS-2               VALUE
              "Requirement/Statement2.txt".
           88 WS-OUTPUT-CUS-3               VALUE
              "Requirement/Statement3.txt".


       01 WS-FILE-STATUS.
           05  WS-100-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-400-FILE-STATUS          PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
      *************************   CALCULATORs **************************
       01 WS-CAL.
           05 WS-REC-COUNT-FILE-1          PIC 9(4)        VALUE ZERO.
           05 WS-OUTPUT-FILE1              PIC 9(4)        VALUE ZERO.
           
           05 WS-SUB                       PIC 9(4)        VALUE ZERO.
           05 WS-SUB-1                     PIC 9(4)        VALUE ZERO.
           05 WS-SUB-2                     PIC 9(4)        VALUE ZERO.
           05 WS-SUB-3                     PIC 9(4)        VALUE ZERO.

           05 WS-POINTER                   PIC 9(4)        VALUE ZERO.

           05 WS-ARRAY-MAX                 PIC 9(4)        VALUE 17.
           05 WS-ARRAY-MAX-1               PIC 9(4)        VALUE 17.

           05 WS-LINE-MAX-FILE1            PIC S9(2)       VALUE +55.
           05 WS-LINE-HEADER-FILE1         PIC S9(3)       VALUE +12.
           05 WS-LINE-COUNT-FILE1          PIC S9(2)       VALUE +55.

           05 WS-TXN-AMOUNT                PIC 9(7)V9(2)   VALUE ZERO.
           05 WS-GRAND-AMOUNT              PIC 9(8)V9(2)   VALUE ZERO.
           05 WS-MIN-PAYMENT               PIC 9(8)V9(2)   VALUE ZERO.

      *************************   CALCULATORs **************************

      *************************   DATE TIME   **************************
       01 WS-DATE-NOW.
           05 YEAR-NOW                     PIC X(4)        VALUE SPACE.
           05 MONTH-NOW                    PIC X(2)        VALUE SPACE.
           05 DAY-NOW                      PIC X(2)        VALUE SPACE.
       01 WS-TIME-NOW.
           05 HOURS-NOW                    PIC X(2)        VALUE SPACE.
           05 MINUTE-NOW                   PIC X(2)        VALUE SPACE.
           05 SECOND-NOW                   PIC X(2)        VALUE SPACE.
      *************************   DATE TIME   **************************

      ************************* HEADER LINE 1 **************************
       01 WS-HEADER-1.
            05 FILLER                      PIC X(45)
               VALUE "REPORT-NO: CR06 ".
            05 FILLER                      PIC X(25)
               VALUE "CUSTOMER STATEMENT REPORT".
            05 FILLER                      PIC X(45)
               VALUE SPACE.
            05 FILLER                      PIC X(7)
               VALUE "DATE : ".
            05 DDAY                        PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE "/".
            05 MONTH                       PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE "/".
            05 YEAR                        PIC X(4).

      ************************* HEADER LINE 1 **************************

      ************************* HEADER LINE 2 **************************
       01 WS-HEADER-2.
            05 FILLER                      PIC X(48)
               VALUE "PROGRAM  : TRR0006".
            05 FILLER                      PIC X(67)
               VALUE SPACE.
            05 FILLER                      PIC X(7)
               VALUE "TIME : ".
            05 HOUR                        PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE ":".
            05 MINUTE                      PIC X(2).
            05 FILLER                      PIC X(1)
               VALUE ":".
            05 SECOND                      PIC X(2).
      ************************* HEADER LINE 2 **************************

      ************************* HEADER DETAIL **************************
       01  WS-HEADER-DETAIL.
           05 WS-ACCOUNT-SUMMARY           PIC X(15)
              VALUE "ACCOUNT SUMMARY".
           05 WS-HEADER-ACCOUNT-SUMMARY.
              10 FILLER                    PIC X(5)        VALUE SPACE.
              10 FILLER                    PIC X(26)
                 VALUE "CARD NUMBER".
              10 FILLER                    PIC X(29)
                 VALUE "ACCOUNT NAME".
              10 FILLER                    PIC X(31)
                 VALUE "CREDIT LIMIT".
              10 FILLER                    PIC X(30)
                 VALUE "BALANCE".
              10 FILLER                    PIC X(11)
                 VALUE "MIN PAYMENT".

           05 WS-ACCOUNT-DETAIL            PIC X(14)
              VALUE "ACCOUNT DETAIL".
           05 WS-HEADER-ACCOUNT-DETAIL.
              10 FILLER                    PIC X(7)        VALUE SPACE.
              10 FILLER                    PIC X(46)
                 VALUE "CARD TYPE".
              10 FILLER                    PIC X(53)
                 VALUE "CARD NUMBER".
              10 FILLER                    PIC X(26)
                 VALUE "ACCOUNT NAME".
           05 WS-HEADER-DESCRIPTION.
              10 FILLER                    PIC X(27)
                 VALUE "TRAN DATE".
              10 FILLER                    PIC X(46)
                 VALUE "POSTING DATE".
              10 FILLER                    PIC X(44)
                 VALUE "DESCRPTION".
              10 FILLER                    PIC X(15)
                 VALUE "AMOUNT  (BAHT)".
          05 WS-TOTAL-LINE.
              10 FILLER                    PIC X(49)       VALUE SPACE.
              10 FILLER                    PIC X(68)
                 VALUE "***** TOTAL BALANCE *****".
              10 WS-TOTAL-BALANCE          PIC ZZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
      ************************* HEADER DETAIL **************************
      

      ************************* FORMAT NUMBER **************************
       01 WS-FORMAT-PHONE-NO.
           05 WS-PHONE-POS1                PIC X(3)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-PHONE-POS2                PIC X(3)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-PHONE-POS3                PIC X(4)        VALUE SPACE.
       01 WS-FORMAT-CARD-ID.
           05 WS-CARD-POS1                 PIC X(4)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE "-".
           05 WS-CARD-POS2                 PIC X(2)        VALUE SPACE.
           05 FILLER                       PIC X(8)    VALUE "XX-XXXX-".
           05 WS-CARD-POS3                 PIC X(4)        VALUE SPACE.
      ************************* FORMAT NUMBER **************************
      *************************     ARRAY     **************************
       01 WS-ARRAY-ALL-RECORD             OCCURS 50 INDEXED BY WS-ID.
           05 WS-CARD-TYPE                 PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CUSTOMER-NAME             PIC X(30)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-CARD-NUMBER               PIC X(19)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-TXN-DATE-INC              PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-POST-DATE                 PIC X(8)        VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-NAME             PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-MERCHANT-ADDRESS-2        PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X           VALUE SPACE.
           05 WS-AMOUNT-INC                PIC 9(8)V99     VALUE ZERO.

      *************************     ARRAY     **************************
       01 WS-CHECK-KEY.
           05 WS-CONTROL-KEY1              PIC X(16)      VALUE SPACE.
           05 WS-CONTROL-KEY2              PIC X(19)      VALUE SPACE.

      ************************* OUTPUT DETAIL **************************
       01 WS-OUTPUT-LINE-1.
           05 WS-OUTPUT-CARD-NUM           PIC X(19)       VALUE SPACE.
           05 FILLER                       PIC X(10)       VALUE SPACE.
           05 WS-OUTPUT-CUSTOMER-NAME      PIC X(16)       VALUE SPACE.
           05 FILLER                       PIC X(17)       VALUE SPACE.
           05 WS-OUTPUT-CREDIT-LIMIT       PIC ZZ,ZZZ,ZZ9  VALUE ZERO.
           05 FILLER                       PIC X(13)       VALUE SPACE.
           05 WS-OUTPUT-BALANCE            PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
           05 FILLER                       PIC X(21)       VALUE SPACE.
           05 WS-OUTPUT-MIN-PAYMENT        PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
       01 WS-OUTPUT-LINE-2.
           05 FILLER                       PIC X(10)       VALUE SPACE.
           05 WS-OUTPUT-CARD-TYPE          PIC X(9)        VALUE SPACE.
           05 FILLER                       PIC X(29)       VALUE SPACE.
           05 WS-OUTPUT-CARD-NUMBER        PIC X(19)       VALUE SPACE.
           05 FILLER                       PIC X(35)       VALUE SPACE.
           05 WS-OUTPUT-CUSTOMER-NAME2     PIC X(30)       VALUE SPACE.
       
       01 WS-OUTPUT-LINE-3.
           05 WS-OUTPUT-TRAN-DATE.
              10 WS-DATE-TRAN              PIC X(2)        VALUE SPACE.
              10 FILLER                    PIC X           VALUE "/".      
              10 WS-MONTH-TRAN             PIC X(2)        VALUE SPACE.
              10 FILLER                    PIC X           VALUE "/".      
              10 WS-YEAR-TRAN              PIC X(2)        VALUE SPACE.
           05 FILLER                       PIC X(19)       VALUE SPACE.
           05 WS-OUTPUT-POST-DATE.
              10 WS-DATE-POST              PIC X(2)        VALUE SPACE.
              10 FILLER                    PIC X           VALUE "/".      
              10 WS-MONTH-POST             PIC X(2)        VALUE SPACE.
              10 FILLER                    PIC X           VALUE "/".      
              10 WS-YEAR-POST              PIC X(2)        VALUE SPACE.
           05 FILLER                       PIC X(13)       VALUE SPACE.
           05 WS-OUTPUT-MERCHANT-NAME      PIC X(14)       VALUE SPACE.
           05 FILLER                       PIC X(17)       VALUE SPACE.
           05 WS-OUTPUT-MERCHANT-ADD       PIC X(18)       VALUE SPACE.
           05 FILLER                       PIC X(21)       VALUE SPACE.
           05 WS-OUTPUT-AMOUNT             PIC ZZ,ZZZ,ZZ9.99
                                                           VALUE ZERO.
      ************************* OUTPUT DETAIL **************************
       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITIAL            THRU 1000-EXIT.
           PERFORM 2000-PROCESS            THRU 2000-EXIT
               UNTIL FILE-AT-END OF WS-100-FILE-STATUS.
           PERFORM 8100-PRINT-LINE       THRU 8100-EXIT.
           PERFORM 3000-END                THRU 3000-EXIT.
           GOBACK.
       1000-INITIAL.
           PERFORM 1200-SELECT-FILE        THRU 1200-EXIT.
           OPEN INPUT  100-INPUT-FILE
                OUTPUT 400-OUTPUT-FILE.
           PERFORM 1100-INITIAL-DATE-TIME  THRU 1100-EXIT.
           PERFORM 8000-READ-FILE          THRU 8000-EXIT.
           DISPLAY 100-CUSTOMER-ID " "
                   100-CARD-NUMBER.
           MOVE 1 TO WS-SUB-1.
       1000-EXIT.
           EXIT.
       1100-INITIAL-DATE-TIME.
           ACCEPT WS-DATE-NOW              FROM DATE YYYYMMDD.
           ACCEPT WS-TIME-NOW              FROM TIME.

           MOVE DAY-NOW                    TO DDAY.
           MOVE MONTH-NOW                  TO MONTH.
           MOVE YEAR-NOW                   TO YEAR.

           MOVE HOURS-NOW                  TO HOUR.
           MOVE MINUTE-NOW                 TO MINUTE.
           MOVE SECOND-NOW                 TO SECOND.
       1100-EXIT.
           EXIT.
       1200-SELECT-FILE.
           PERFORM UNTIL WS-INPUT-IS-TRUE
               DISPLAY "ENTER NUMBER FILE -  "
               DISPLAY "1. STATEMENT CUSTOMER 1"
               DISPLAY "2. STATEMENT CUSTOMER 2"
               DISPLAY "3. STATEMENT CUSTOMER 3"

               ACCEPT WS-NAME-OF-FILE
               EVALUATE WS-NAME-OF-FILE
                   WHEN "1"
                       SET   WS-CUSTOMER-1
                             WS-INPUT-IS-TRUE 
                             WS-OUTPUT-CUS-1 
                       TO TRUE
                   WHEN "2"
                       SET   WS-CUSTOMER-2
                             WS-INPUT-IS-TRUE     
                             WS-OUTPUT-CUS-2   
                       TO TRUE
                   WHEN "3"
                       SET   WS-CUSTOMER-3
                             WS-INPUT-IS-TRUE        
                             WS-OUTPUT-CUS-3
                       TO TRUE
                   WHEN OTHER
                       DISPLAY "INPUT INCORRECT !!!"
               END-EVALUATE
               DISPLAY WS-NAME-OF-FILE
           END-PERFORM.
       1200-EXIT.
           EXIT.
       2000-PROCESS.
           PERFORM 4000-INITIAL            THRU 4000-EXIT.
           PERFORM 4500-DETAIL-LINE        THRU 4500-EXIT
              UNTIL WS-CONTROL-KEY1 NOT = 100-CARD-NUMBER-TYPE-INC
                    OR FILE-AT-END OF WS-100-FILE-STATUS.
           PERFORM 4600-MOVE-DETAIL      THRU 4600-EXIT.
           
       2000-EXIT.
           EXIT.
       3000-END.
           DISPLAY "----------- END OF FILE -----------".
           DISPLAY "INPUT  FILE 1 : " WS-REC-COUNT-FILE-1.
           DISPLAY "OUTPUT FILE "WS-OUTPUT-FILE-NAME": " WS-OUTPUT-FILE1.
           DISPLAY "-----------------------------------".
           
                 MOVE 2 TO WS-SUB.
                 MOVE ALL SPACE                TO 400-OUTPUT-RECORD
                 PERFORM 7000-WRITE-FILE-1     THRU 7000-EXIT
                 
                 MOVE WS-ACCOUNT-DETAIL TO 400-OUTPUT-RECORD
                 PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
                 PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                 MOVE WS-HEADER-ACCOUNT-DETAIL TO 400-OUTPUT-RECORD
                 PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
                 PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                 PERFORM 4700-MOVE-ACCOUNT-DETAIL THRU 4700-EXIT
                 PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                 MOVE WS-HEADER-DESCRIPTION TO 400-OUTPUT-RECORD
                 PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
                 PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
           
           MOVE WS-CARD-NUMBER(2)      TO WS-CONTROL-KEY2
           PERFORM VARYING WS-SUB           FROM 2 BY +1 
              UNTIL WS-SUB > WS-SUB-1
                 IF WS-CARD-NUMBER(WS-SUB) = WS-CONTROL-KEY2
                    PERFORM 4800-MOVE-DETAIL-CARD THRU 4800-EXIT
                 ELSE
                    
                    MOVE WS-GRAND-AMOUNT          TO WS-TOTAL-BALANCE
                    MOVE WS-TOTAL-LINE            TO 400-OUTPUT-RECORD
                    PERFORM 7000-WRITE-FILE-1     THRU 7000-EXIT
                    MOVE ALL SPACE                TO 400-OUTPUT-RECORD
                    PERFORM 7000-WRITE-FILE-1     THRU 7000-EXIT
                    MOVE ZERO                     TO WS-GRAND-AMOUNT

                    PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                    MOVE WS-HEADER-ACCOUNT-DETAIL TO 400-OUTPUT-RECORD
                    PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
                    PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                    PERFORM 4700-MOVE-ACCOUNT-DETAIL THRU 4700-EXIT
                    PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                    MOVE WS-HEADER-DESCRIPTION TO 400-OUTPUT-RECORD
                    PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
                    PERFORM 8100-PRINT-LINE       THRU 8100-EXIT
                    
                    PERFORM 4800-MOVE-DETAIL-CARD THRU 4800-EXIT
                 END-IF
             
              MOVE WS-CARD-NUMBER(WS-SUB) TO WS-CONTROL-KEY2
           END-PERFORM.                 
           MOVE WS-GRAND-AMOUNT          TO WS-TOTAL-BALANCE
           MOVE WS-TOTAL-LINE TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE-1 THRU 7000-EXIT
           CLOSE 100-INPUT-FILE
                 400-OUTPUT-FILE.
       3000-EXIT.
           EXIT.
       4000-INITIAL.

           MOVE 100-CARD-NUMBER-TYPE-INC   TO WS-CONTROL-KEY1.
           
       4000-EXIT.
           EXIT.

       4500-DETAIL-LINE.
           PERFORM 6000-CHACK-HEADER       THRU 6000-EXIT.
      *************************** MOVE ARRAY ***************************
           ADD +1 TO WS-SUB-1.
           MOVE 100-CARD-TYPE            TO WS-CARD-TYPE(WS-SUB-1).
           MOVE 100-CUSTOMER-NAME        TO WS-CUSTOMER-NAME(WS-SUB-1).
           MOVE WS-FORMAT-CARD-ID        TO WS-CARD-NUMBER(WS-SUB-1).
           MOVE 100-TXN-DATE-INC         TO WS-TXN-DATE-INC(WS-SUB-1).
           MOVE WS-DATE-NOW              TO WS-POST-DATE(WS-SUB-1).
           MOVE 100-MERCHANT-NAME        TO WS-MERCHANT-NAME(WS-SUB-1).
           MOVE 100-MERCHANT-ADDRESS-2   TO 
                                      WS-MERCHANT-ADDRESS-2(WS-SUB-1).
           MOVE 100-AMOUNT-INC           TO WS-AMOUNT-INC(WS-SUB-1).
           
      ******************************************************************
           
           PERFORM 8000-READ-FILE          THRU 8000-EXIT.
       4500-EXIT.
           EXIT.
       4600-MOVE-DETAIL.
           MOVE WS-GRAND-AMOUNT             TO WS-OUTPUT-BALANCE.
           COMPUTE WS-MIN-PAYMENT = WS-GRAND-AMOUNT * 0.1 .
           MOVE WS-MIN-PAYMENT              TO WS-OUTPUT-MIN-PAYMENT.

           MOVE WS-OUTPUT-LINE-1            TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-1        THRU 7000-EXIT.
           MOVE ZERO                        TO WS-GRAND-AMOUNT.
       4600-EXIT.
           EXIT.
       4700-MOVE-ACCOUNT-DETAIL.
           MOVE WS-CARD-TYPE(WS-SUB)        TO WS-OUTPUT-CARD-TYPE.
           MOVE WS-CARD-NUMBER (WS-SUB)     TO WS-OUTPUT-CARD-NUMBER.
           MOVE WS-CUSTOMER-NAME(WS-SUB)    TO WS-OUTPUT-CUSTOMER-NAME2.

           MOVE WS-OUTPUT-LINE-2            TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-1        THRU 7000-EXIT.
       4700-EXIT.
           EXIT.
       4800-MOVE-DETAIL-CARD.
           MOVE WS-TXN-DATE-INC(WS-SUB)(1:2)     TO WS-DATE-TRAN.
           MOVE WS-TXN-DATE-INC(WS-SUB)(3:2)     TO WS-MONTH-TRAN.
           MOVE WS-TXN-DATE-INC(WS-SUB)(7:2)     TO WS-YEAR-TRAN.

           MOVE WS-DATE-NOW(7:2)       TO WS-DATE-POST.
           MOVE WS-DATE-NOW(5:2)       TO WS-MONTH-POST.
           MOVE WS-DATE-NOW(3:2)       TO WS-YEAR-POST.

           MOVE WS-MERCHANT-NAME(WS-SUB)    TO WS-OUTPUT-MERCHANT-NAME.

           MOVE WS-MERCHANT-ADDRESS-2(WS-SUB) 
                TO WS-OUTPUT-MERCHANT-ADD.

           MOVE WS-AMOUNT-INC(WS-SUB)       TO WS-OUTPUT-AMOUNT.
           ADD WS-AMOUNT-INC(WS-SUB)        TO WS-GRAND-AMOUNT.
          
           MOVE WS-OUTPUT-LINE-3            TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-1        THRU 7000-EXIT.
       4800-EXIT.
           EXIT.
       6000-CHACK-HEADER.
           ADD +1                          TO WS-LINE-COUNT-FILE1.
           IF WS-LINE-COUNT-FILE1 >= WS-LINE-MAX-FILE1
               MOVE WS-HEADER-1            TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE WS-HEADER-2            TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT

               MOVE  100-CUSTOMER-NAME     TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT

               PERFORM 7900-FORMATE-CUS-PHONE-NO THRU 7900-EXIT
               MOVE WS-FORMAT-PHONE-NO         TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT

               PERFORM 7600-FORMAT-STRING  THRU 7600-EXIT
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE 100-CUSTOMER-ZIP-CODE  TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE ALL SPACE              TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT

               MOVE WS-ACCOUNT-SUMMARY     TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE ALL "-"                TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE WS-HEADER-ACCOUNT-SUMMARY
                                           TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT
               MOVE ALL "-"                TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-1   THRU 7000-EXIT              
               MOVE WS-LINE-HEADER-FILE1    TO WS-LINE-COUNT-FILE1
           END-IF.

           PERFORM 7500-FORMAT-CARD-ID      THRU 7500-EXIT.
           MOVE WS-FORMAT-CARD-ID           TO WS-OUTPUT-CARD-NUM.
           MOVE 100-CUSTOMER-NAME           TO WS-OUTPUT-CUSTOMER-NAME.
           MOVE 100-CARD-CREDIT-LIMIT       TO WS-OUTPUT-CREDIT-LIMIT.
           ADD 100-AMOUNT-INC               TO WS-GRAND-AMOUNT.
           

       6000-EXIT.
           EXIT.
       7000-WRITE-FILE-1.
           WRITE 400-OUTPUT-RECORD.
           IF FILE-OK OF WS-400-FILE-STATUS
               ADD +1                      TO WS-OUTPUT-FILE1
               CONTINUE
           ELSE
               DISPLAY "FILE STATUS 4 : " WS-400-FILE-STATUS
               PERFORM 9000-ABEND          THRU 9000-EXIT
           END-IF.
       7000-EXIT.
           EXIT.
       7500-FORMAT-CARD-ID.
           MOVE 100-CARD-NUMBER(1:4)       TO WS-CARD-POS1.
           MOVE 100-CARD-NUMBER(5:2)       TO WS-CARD-POS2.
           MOVE 100-CARD-NUMBER(13:4)      TO WS-CARD-POS3.
       7500-EXIT.
           EXIT.
       7600-FORMAT-STRING.
           STRING  100-CUSTOMER-ADDRESS-1 DELIMITED BY SIZE
                   100-CUSTOMER-ADDRESS-2 DELIMITED BY SIZE
                   INTO 400-OUTPUT-RECORD
           END-STRING.
       7600-EXIT.
           EXIT.
       7900-FORMATE-CUS-PHONE-NO.
           MOVE 100-CUSTOMER-PHONE-NUMBER(1:3)
                                           TO WS-PHONE-POS1.
           MOVE 100-CUSTOMER-PHONE-NUMBER(4:3)
                                           TO WS-PHONE-POS2.
           MOVE 100-CUSTOMER-PHONE-NUMBER(7:4)
                                           TO WS-PHONE-POS3.
       7900-EXIT.
           EXIT.
       8000-READ-FILE.
           READ 100-INPUT-FILE.
           IF FILE-OK                      OF WS-100-FILE-STATUS
               ADD +1                      TO WS-REC-COUNT-FILE-1
               CONTINUE
           ELSE
               IF FILE-AT-END              OF WS-100-FILE-STATUS
                       CONTINUE
               ELSE
                   DISPLAY "FILE STATUS 1 : " WS-100-FILE-STATUS
                   PERFORM 9000-ABEND      THRU 9000-EXIT
               END-IF
           END-IF.
       8000-EXIT.
           EXIT.
       8100-PRINT-LINE.
           ADD +1                        TO WS-REC-COUNT-FILE-1.
           MOVE ALL "-"                  TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-1     THRU 7000-EXIT.
       8100-EXIT.
           EXIT.
       9000-ABEND.
           MOVE 12 TO RETURN-CODE.
           GOBACK.
       9000-EXIT.
           EXIT.
